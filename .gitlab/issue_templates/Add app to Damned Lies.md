<!-- Use the following format for your issue title:

Add app-name to Damned Lies

-->

## General

<!--
This is a template for apps that are not part of GNOME Core, Incubator, or Circle.
If your app is in one of these groups, inclusion on Damned Lies comes as a perk.
The i18n Coordination Team doesn’t need to make a decision, file an issue directly against Damned Lies:
https://gitlab.gnome.org/Infrastructure/damned-lies/-/issues/new
-->

App repository: <!-- example: https://gitlab.gnome.org/maint/app -->

* [ ] Request is made by one of the maintainers <!-- Mandatory -->

## Development state

<!-- These are not hard requirements and just help us get a feel before we make a decision -->

* [ ] App uses current platform technology
* [ ] App is available on Flathub
* [ ] Releases are published regularly <!-- We recommend two to six months between releases -->

## Expectations

* [ ] Strings follow the [Human Interface Guidelines on Typography](https://developer.gnome.org/hig/guidelines/typography.html)

⚠️ Developers should *never* touch po files.
If changes are made to strings, they should only be made in source code.
Tooling will apply the changes to po files, marking strings as fuzzy or untranslated as necessary.
Translators will then review them and make the appropriate changes.

⚠️ Translations must only come from Damned Lies.
Merge requests must no longer be accepted.
Developers must not pull translations from other platforms either.
Damned Lies as the sole translation platform for the project ensures no work is duplicated and no effort is wasted.

ℹ️ Releases are announced in advance on [Discourse](https://discourse.gnome.org/) (App category, i18n tag).
A period of around two weeks is generally considered good.
This ensures translators know when it’s the right time to focus on the app, and have time to do the work.
We recommend you refrain from making any changes to strings at this point until after the release, but fixing issues with strings themselves is probably still a good idea, as problematic strings will make translation work more difficult if not impossible.

* [ ] I have read the above expectations and will do my best to meet them
* [ ] I have given @translations the Developer role
