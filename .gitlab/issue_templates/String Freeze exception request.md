<!-- Use the following format for your issue title:

String freeze exception request, module-name

-->

<!--
Please fill in this template with as much details as possible when filing the request.
The less coordinators have to dig them up, the quicker they will approve it.
-->

## Rationale

We have implemented *summary of the feature*…
<!-- or maybe -->
We have fixed *summary of the issue*…

As a consequence, *n* new strings would appear in the application.

## Strings

<!--
It is important that we are able to review the strings.
This helps decide whether it is reasonable to ask translators to work on them, considering all other constraints.
This also gives us a chance to catch any potential obstacle to properly translate them.
Please don’t make us dig through the code to find out what they are.
-->

The new strings are…


/label string_freeze
